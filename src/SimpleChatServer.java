import java.io.*;
import java.net.*;
import java.util.*;

public class SimpleChatServer {

    ArrayList clientOutputStreams;

    public static void main(String[] args) {
        new SimpleChatServer().go();
    }

    public void go()
    {
        clientOutputStreams = new ArrayList();
        try
        {
            ServerSocket serverSocket = new ServerSocket(5057); // подняли порт сервера для клиентских соединений

            while (true)
            {
                Socket clientSocket = serverSocket.accept(); // слушаем подключения клиентов
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                clientOutputStreams.add(writer); // добавили в массив объект PrintWriter для текущего клиента

                Thread t = new Thread(new ClientHandler(clientSocket)); // Каждому новому соединению свой поток
                t.start();
                System.out.println("got a connection");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public class ClientHandler implements Runnable
    {
        BufferedReader reader;
        Socket socket;

        public ClientHandler(Socket clientSocket)
        {
            try
            {
                socket = clientSocket;
                InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
                reader = new BufferedReader(isReader); // создаем слушателя входящего потока
            }
            catch (Exception ex) {ex.printStackTrace();}


        }

        public void run()
        {
            String message;
            try
            {
                while ((message = reader.readLine()) != null)
                {
                    System.out.println("Получено от клиента: " + message);
                    tellEveryone(message); // Отослать всем зарегистрированным клиентам
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }


    //-- Отсылка сообщений всем подключенным пользователям из clientOutputStreams
    public void tellEveryone(String message)
    {
        Iterator it = clientOutputStreams.iterator();
        while (it.hasNext())
        {
            try
            {
                PrintWriter writer = (PrintWriter) it.next();
                writer.println(message);
                writer.flush();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

}
